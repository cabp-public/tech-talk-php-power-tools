<?php

namespace Machine;

use DataReader\DataReader;
use Machine\Models\AwarenessModel;
use Rubix\ML\Datasets\Unlabeled;


class Machine
{
    private $dataReader;
    private $modelsAvail;

    function __construct(DataReader $dataReader)
    {
        $this->dataReader = $dataReader;
        $this->createModels();
    }

    private function createModels()
    {
        try {
            // We could use recursion and create models dynamically
            $this->modelsAvail = [
                'Awareness' => new AwarenessModel(),
            ];
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
    }

    public function trainFromFiles()
    {
        $output = false;

        try {
            $output = true;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function trainModelFromFile(string $modelName, $index)
    {
        $output = false;

        try {
            $model = $this->modelsAvail[$modelName];
            $sourcePath = 'training/' . strtolower($modelName) . '_' . $index;
            $learnedPath = 'learned/' . strtolower($modelName) . '_' . $index;

            if ($this->dataReader->dataAvailable($sourcePath)) {
                $trainingData = $this->dataReader->getDataFromFile($sourcePath, 'DIVISION');
                $output = $model->trainWithData($trainingData['samples'], $trainingData['labels']);
//                $this->dataReader->moveFile($sourcePath, $learnedPath);
            }
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function predictModel(string $modelName, $index)
    {
        $output = false;

        try {
            $model = $this->modelsAvail[$modelName];
            $sourcePath = 'prediction/' . strtolower($modelName) . '_' . $index;
            $outputPath = 'predicted/' . time();

            if ($this->dataReader->dataAvailable($sourcePath)) {
                $predictionData = $this->dataReader->getDataToPredictFromFile($sourcePath);
                $output = $model->predictFromUnlabeled($predictionData);
                $this->dataReader->savePrediction($outputPath, $output);
                $output = true;
            }
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function predictAllModels()
    {
        $output = false;

        try {
            $output = true;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }
}