<?php

namespace Machine\Models;

use Rubix\ML\Classifiers\KNearestNeighbors;
use Rubix\ML\Datasets\Labeled;


class DefaultKNN
{
    protected $k;
    protected $dataset;
    protected $estimator;

    function __construct(int $k = 5, array $samples = [], array $labels = [])
    {
        $this->k = $k;
        $this->dataset = new Labeled($samples, $labels);
        $this->estimator = new KNearestNeighbors($k);
    }

    protected function remakeEstimator(int $k)
    {
        $output = false;

        try {
            $this->k = $k;
            $this->estimator = new KNearestNeighbors($this->k);
            $output = true;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    protected function makeLabeledDatasetFromArray(array $samples, array $labels)
    {
        $output = false;

        try {
            $this->dataset = new Labeled($samples, $labels);
            $output = true;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    protected function setLabeledDataset($samples, $labels)
    {
        $output = false;

        try {
            $this->dataset = Labeled::fromIterator($samples, $labels);
            $output = true;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }
}