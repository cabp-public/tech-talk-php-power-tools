<?php

namespace Machine\Models;


use Rubix\ML\Datasets\Unlabeled;

class AwarenessModel extends DefaultKNN
{
    function __construct($k = 5, array $data = [], array $labels = [])
    {
        parent::__construct($k, $data, $labels);
    }

    public function trainWithData($samples, $labels)
    {
        $output = false;

        try {
            $this->makeLabeledDatasetFromArray($samples, $labels);
            $this->estimator->train($this->dataset);
            $output = $this->estimator->trained();
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function predictFromUnlabeled(array $data)
    {
        $output = false;

        try {
            $dataset = new Unlabeled($data);
            $output = $this->estimator->predict($dataset);
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }
}