<?php

namespace Events;

use Machine\Machine;
use React\EventLoop\TimerInterface;


class LearningLoop extends Looper
{
    private $machine;
    private $trainingPaths;
    private $predictionTime;
    private $predictionInterval;

    function __construct(int $interval, int $predictionInterval, Machine $machine)
    {
        $this->predictionTime = $predictionInterval;
        $this->predictionInterval = $predictionInterval;
        $this->machine = $machine;

        parent::__construct($interval, [$this, 'periodicTimerCallBack']);

        echo "\nIter\tModel\t\tTrained\t\tPrediction\n";
    }

    private function saveLogs()
    {}

    private function cleanSource()
    {}

    public function periodicTimerCallBack(TimerInterface $timer)
    {
        try {
            $modelName = 'Awareness';
            $modelTrained = false;
            $predictionReady = false;

            echo $this->timeCount ."\t";
            echo $modelName ."\t";

            if (intval($this->timeCount) === intval($this->predictionTime)) {
                $predictionResults = $this->machine->predictModel($modelName, $this->timeCount);
                $predictionReady = true;

//                echo "\ttrue\n";
//                var_dump($predictionResults);
//                echo "\n";

                $this->predictionTime += $this->predictionInterval;
            }
            else {
                $modelTrained = $this->machine->trainModelFromFile($modelName, $this->timeCount);
            }

            echo intval($modelTrained) . "\t\t" . intval($predictionReady) . "\n";

            $this->timeCount++;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
    }
}