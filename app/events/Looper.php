<?php

namespace Events;

use Machine\Machine;
use React\EventLoop\Factory as ReactFactory;
use React\EventLoop\TimerInterface as TimerInterface;


class Looper
{
    protected $eventLoop;
    protected $eventTimer;
    protected $timeCount;

    function __construct(float $interval, array $callback)
    {
        $this->timeCount = 0;
        $this->eventLoop = ReactFactory::create();
        $this->eventTimer = $this->eventLoop->addPeriodicTimer($interval, $callback);
//        $this->setPeriodicTimer($interval);
    }

    private function setPeriodicTimer(float $interval)
    {
        try {
            $this->eventTimer = $this->eventLoop->addPeriodicTimer($interval, [$this, 'periodicTimerCallBack']);
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
    }

    /*public function periodicTimerCallBack(TimerInterface $timer)
    {
        try {
            echo "Time: $this->timeCount\n";
            $this->timeCount++;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
    }*/

    public function start()
    {
        try {
            $this->eventLoop->run();
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
    }

    /**
     * Stop the loop
     * @param array $reason
     * @return bool
     */
    public function stop(array $reason): bool
    {
        $output = false;

        try {
            echo "SOPT: $this->timeCount\n";
            $this->eventLoop->cancelTimer($this->eventTimer);
            $output = true;
        }
        catch (\Exception $exception) {
        }
        finally {
            return $output;
        }
    }
}