<?php

namespace DataReader;


use League\Csv\Reader;

class DataReader
{
    private $dataPath;
    private $dataType;

    function __construct(string $dataPath, string $type = 'csv')
    {
        $this->dataPath = $dataPath;
        $this->dataType = $type;
    }

    public function setType(string $type)
    {
        $this->dataType = $type;
    }

    public function dataDirAvailable(string $relativePath)
    {
        $output = false;

        try {
            $fullPath = $this->dataPath . $relativePath;

            if (is_dir($fullPath)) {
                $files = scandir($fullPath);
                array_shift($files);
                array_shift($files);

                $output = count($files) > 0;
            }
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function dataAvailable(string $relativePath)
    {
        $output = false;

        try {
            $fullPath = $this->dataPath . $relativePath . '.' . $this->dataType;
            $output = file_exists($fullPath);
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function moveFile(string $source, string $target)
    {
        $output = false;

        try {
            $from = $this->dataPath . $source . '.' . $this->dataType;
            $to = $this->dataPath . $target . '.' . $this->dataType;

            rename($from, $to);
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function getDataFromFile(string $relativePath, string $labelCol)
    {
        $output = false;

        try {
            $fullPath = $this->dataPath . $relativePath . '.' . $this->dataType;
            $csv = array_map('str_getcsv', file($fullPath));

            $samples = [];
            $labels = [];

            foreach ($csv as $row) {
                $labels[] = strtoupper($row[0]);
                $samples[] = [
                    floatval($row[1]),
                    floatval($row[2]),
                    floatval($row[3])
                ];
            }

            $output = [
                'samples' => $samples,
                'labels' => $labels,
            ];
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function getDataToPredictFromFile(string $relativePath)
    {
        $output = false;
        $samples = [];

        try {
            $fullPath = $this->dataPath . $relativePath . '.' . $this->dataType;
            $csv = array_map('str_getcsv', file($fullPath));

            foreach ($csv as $row) {
                $samples[] = [
                    floatval($row[0]),
                    floatval($row[1]),
                    floatval($row[2])
                ];
            }

            $output = $samples;
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }

    public function savePrediction(string $relativePath, array $data)
    {
        $output = false;
        $fullPath  = $this->dataPath . $relativePath . '.json';

//        var_dump($fullPath);

        try {
            file_put_contents($fullPath, json_encode($data));
        }
        catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
        finally {
            return $output;
        }
    }
}