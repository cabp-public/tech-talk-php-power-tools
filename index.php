<?php

require 'vendor/autoload.php';

// Data Reader
$dataPath = __DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
$dataReader = new \DataReader\DataReader($dataPath);

// Our Machine
$machine = new \Machine\Machine($dataReader);

// We create an event loop to read data constantly
$minutes = 5;
$learningInterval = $minutes * 60;
$predictionInterval = 3;

$learningLoop = new \Events\LearningLoop($learningInterval, $predictionInterval, $machine);
$learningLoop->start();
